from django.conf.urls import url
from .views import index, about, register

app_name = 'story_3'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^about/', about, name='about'),
    url(r'^register/', register, name='register'),
]