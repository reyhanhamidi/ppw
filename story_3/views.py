from django.shortcuts import render

# Create your views here.
response = {}

def index(request):
    return render(request, 'story_3_home.html', response)

def about(request):
    return render(request, 'story_3_about.html', response)
    
def register(request):
    return render(request, 'story_3_register.html', response)