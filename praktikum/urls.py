"""praktikum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
# import story_1.urls as story_1
import story_3.urls as story_3
import story_5.urls as story_5

urlpatterns = [
    path('admin/', admin.site.urls),
    # url(r'^', include(story_1, namespace='story-1')), 
    url(r'^', include(story_3, namespace='story-3')),
    url(r'^schedule/', include(story_5, namespace='story-5')),
]
