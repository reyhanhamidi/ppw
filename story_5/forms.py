from django import forms
# import datetime

class Schedule_Form(forms.Form):
    attrs = {'class':'form-control'}
    attrs_date = {'type':'date'}
    attrs_time = {'type':'time'}
    name = forms.CharField(label='Activity Name', max_length=50, required=True,
        widget=forms.TextInput(attrs=attrs))
    date = forms.DateField(label='Date', required=False,
        widget=forms.DateInput(attrs={**attrs, **attrs_date}))
    time = forms.TimeField(label='Time', required=False,
        widget=forms.TimeInput(attrs={**attrs, **attrs_time}))
    place = forms.CharField(label='Place', max_length=100, required=True,
        widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='Category', max_length=75, required=True,
        widget=forms.TextInput(attrs=attrs))
