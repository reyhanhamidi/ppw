from django.db import models
from datetime import datetime
from django.utils import timezone

# Create your models here.
class Activity(models.Model):
    name = models.CharField(max_length=50)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=75)