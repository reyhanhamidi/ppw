from django.shortcuts import render
from django.http import HttpResponseRedirect
from datetime import datetime, date
from .forms import Schedule_Form
from .models import Activity

# Create your views here.
response = {}
html = 'story_5_jadwal.html'


def index(request):
    form = Schedule_Form()
    response['form'] = form

    activity_list = Activity.objects.all().values()
    activity_dict = convert_queryset_into_json(activity_list)
    response['activity_dict'] = activity_dict[::-1]

    return render(request, html, response)

def add_activity(request):
    form = Schedule_Form(request.POST or None)
    response['form'] = form
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['date'] = request.POST['date'] if request.POST['date'] != '' else datetime.now().strftime("%Y-%m-%d")
        response['time'] = request.POST['time'] if request.POST['time'] != '' else datetime.now().strftime("%X")
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        activity = Activity(name=response['name'],
            date=response['date'],
            time=response['time'],
            place=response['place'],
            category=response['category']
        )
        activity.save()
        return HttpResponseRedirect('/schedule')
    else:
        return HttpResponseRedirect('/')

def delete_all(request):
    Activity.objects.all().delete()
    return HttpResponseRedirect('/schedule')
        
def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val