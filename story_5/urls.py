from django.conf.urls import url
from .views import index, add_activity, delete_all

app_name = 'story_5'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_activity', add_activity, name='add_activity'),
    url(r'^delete_all', delete_all, name='delete_all'),
]